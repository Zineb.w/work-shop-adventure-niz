# Ajout des jobs de déploiement

## Configurer les "builds" du runner

Créer un fichier .gitlab-ci.yml avec pour les différents types de déploiment les stages associés.

Dans les exemples suivant, vous pouvez constater qu'il y a une ancre '.kubeconfig' (job non exécuté) qui sera étendu par tous les jobs qui font du déploiements dans kube.  
Ce '.kubeconfig' permet d'utiliser un container avec tous les outils utiles (kubectl, helm, kustomize, dhall en choisissant l'image de tag 1.1.0) dans lequels ont injecte la chaine de connexion au cluster (le fichier ~/.kube/config)

Ce `.gitlab-ci.yml` contient des stages pour plusieurs types de déploiements, il faut utiliser ce qui correspont à votre besoin

```yaml
.kubeconfig:
  image: louiznk/k8s-tools:1.0.0
  before_script:
  - |
    mkdir -p ${HOME}/.kube
    mv ${KUBECONFIG_FILE} ${HOME}/.kube/config

stages:
- **-dev

# exemple pour helm
helm-dev:
  stage: deploy-helm-dev
  extends: .kubeconfig
  script:
    - echo "👋 Deploy helm dans le namespace helm-dev"
    - kubectl create ns helm-dev 2>/dev/null || true
    - helm template sith-dev --create-namespace -n helm-dev -f 02-helm/values-staging.yaml --set testConnection=false 02-helm/ | kubectl apply -n helm-dev -f -


# exemple pour kustomize
kustomize-dev:
  stage: deploy-kustomize-dev
  extends: .kubeconfig
  script:
    - echo "👋 Deploy kustomize dans le namespace kustomize-dev"
    - kubectl create ns kustomize-dev 2>/dev/null || true
    - kustomize build 02-kustomize/overlays/staging/ | kubectl apply -n kustomize-dev -f -

# exemple pour dhall
## pour utiliser dhall il faut dans ".kubeconfig" utiliser l'image louiznk/k8s-tools:1.1.0
dhall-dev:
  stage: deploy-dhall-dev
  extends: .kubeconfig
  script:
    - echo "👋 Deploy dhall dans le namespace dhall-dev"
    - kubectl create ns dhall-dev 2>/dev/null || true
    - dhall-to-yaml --documents --file 02-dhall/assembly-staging.dhall | kubectl apply -n dhall-dev -f -
```

Il manque malheureusement la boucle de contrôle pour avoir quelque chose qui réponde pleinement au besoin et s'approche des fonctionnalités d'ArgoCD mais vous n'avez pas le temps de faire cela dans les temps. Ce sera donc vous la boucle de contrôle.

Remarque : pour les ateliers pour faire au plus simple et au plus rapide nous n'utiliserons pas de branches (mais normalement il faut le faire avec des actions différentes en fonction des branches).
