#!/bin/bash
set -e

DIR=$(dirname "$0")
pushd $DIR

echo "Get public cert for sealing secret"

echo "📦 Save public cert in $(pwd)/public-cert.pem"

# si pb pour fetch le certif => le certif est dans un secret kube
set -x
kubeseal --fetch-cert > public-cert.pem

{ set +x; } 2> /dev/null # silently disable xtrace
popd